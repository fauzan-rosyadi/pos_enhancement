{
    'name': 'POS enhancement',
    'version': '1.0',
    'category': 'Point of Sale',
    'description': """
        This module is used to manage consignment.
    """,
    'author': 'Fauzan',
    "website": "",
    'depends': ['base', 'product', 'point_of_sale', 'l10n_id_kw'],
    'data': [
        'security/ir.model.access.csv',
        'data/pos_data.xml',
        # 'data/account.journal.csv',
        'wizard/pos_create_cashier.xml',
        'wizard/pos_update_receipt.xml',
        'views/order_filter.xml',
        'views/pos_details.xml'
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
