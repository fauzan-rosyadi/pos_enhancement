from openerp import models, fields, api


class PosUpdateReceipt(models.TransientModel):
    _name = 'pos.receipt.wizard'

    @api.model
    def get_main_header(self):
        pos_obj = self.env['pos.config'].search([])[0]
        return pos_obj.receipt_header

    @api.model
    def get_main_footer(self):
        pos_obj = self.env['pos.config'].search([])[0]
        return pos_obj.receipt_footer

    header = fields.Text('Header', default=get_main_header)
    footer = fields.Text('Footer', default=get_main_footer)

    @api.multi
    def update_receipt(self):
        pos_obj = self.env['pos.config'].search([])
        vals = {
            'receipt_header': self.header,
            'receipt_footer': self.footer
        }
        pos_obj.write(vals)
        return True
