from openerp import models, fields, api


class PosCreateCashier(models.TransientModel):
    _name = 'pos.cashier.wizard'

    name = fields.Char('Name')
    login = fields.Char('Login')
    password = fields.Char('Password')

    @api.multi
    def create_cashier(self):
        user_obj = self.env['res.users']
        partner_obj = self.env['res.partner']
        session_obj = self.env['pos.session']
        pos_group_user = self.env.ref('point_of_sale.group_pos_user')
        pos_obj = self.env['pos.config'].search([])[0]

        config_vals = {
            'name': self.name,
            'iface_tax_included': True,
            'receipt_header': pos_obj.receipt_header,
            'receipt_footer': pos_obj.receipt_footer
        }
        new_pos_id = pos_obj.create(config_vals)

        partner_vals = {
            'name': self.name,
            'email': '%s@email.com' % self.name
        }
        partner_id = partner_obj.create(partner_vals)

        user_vals = {
            'partner_id': partner_id.id,
            'login': self.login,
            'password': self.password,
            'pos_config': new_pos_id.id,
            'groups_id': [(4, pos_group_user.id)]
        }
        user_id = user_obj.create(user_vals)

        session_vals = {
            'user_id': user_id.id,
            'config_id': new_pos_id.id,
        }
        session_obj.create(session_vals)

        return True
