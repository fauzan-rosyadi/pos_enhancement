from openerp import models, api


class PosConfig(models.Model):
    _inherit = 'pos.config'

    @api.model
    def search_read(self, domain=None, fields=None,
                    offset=0, limit=None, order=None):
        group_id = self.env.user.groups_id
        manager_id = self.env.ref('point_of_sale.group_pos_manager')
        if manager_id not in group_id:
            # kasir hanya bisa melihat PoS sessionnya sendiri
            pos_config_id = self.env.user.pos_config.id
            domain = [('id', '=', pos_config_id)]
        res = super(PosConfig, self).search_read(
            domain=domain, fields=fields, offset=offset, limit=limit,
            order=order)
        return res