import datetime
from openerp import models, fields, api


class PosDetails(models.TransientModel):
    _inherit = 'pos.details'

    tipe = fields.Selection(
        [('1', 'Daily'), ('2', 'Weekly'), ('3', 'Monthly')],
        'Report Type', default='1')

    @api.onchange('tipe')
    def ganti_tanggal(self):
        if self.tipe == '2':
            today = fields.Date.from_string(fields.Date.context_today(self))
            week = datetime.timedelta(weeks=1)
            last_week = today - week
            self.date_start = fields.Date.to_string(last_week)
        elif self.tipe == '3':
            today = fields.Date.from_string(fields.Date.context_today(self))
            month = datetime.timedelta(days=30)
            last_month = today - month
            self.date_start = fields.Date.to_string(last_month)
        else:
            self.date_start = fields.Date.context_today(self)
